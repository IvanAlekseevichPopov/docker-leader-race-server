FROM php:7.0-fpm

RUN apt-get update && apt-get install -y \
    git \
    libicu-dev \
    libpq-dev \
    locales \
    locales-all \
    zlib1g-dev

RUN docker-php-ext-install \
    bcmath \
    intl \
    opcache \
    pdo \
    pdo_pgsql \
    zip

#Liip Imagine support
RUN apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        libpng-dev \
    && docker-php-ext-install -j$(nproc) iconv mcrypt \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd

#RUN pecl install redis; \
#    rm -rf /tmp/pear; \
#    echo "extension=redis.so" > /usr/local/etc/php/conf.d/redis.ini
RUN curl -L -o /tmp/redis.tar.gz https://pecl.php.net/get/redis-3.1.6.tgz \
    && tar xfz /tmp/redis.tar.gz \
    && rm -r /tmp/redis.tar.gz \
    && mkdir -p /usr/src/php/ext \
    && mv redis-3.1.6 /usr/src/php/ext/redis \
    && docker-php-ext-install redis

ENV COMPOSER_ALLOW_SUPERUSER 1

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
    && chmod +x /usr/local/bin/composer

RUN usermod -s /bin/bash www-data; \
    mkdir -p /var/www/history; \
    touch /var/www/history/bash_history_php; \
    chmod -R 777 /var/www/history

ENV PROMPT_COMMAND="history -a"
ENV HISTTIMEFORMAT="%h %d %H:%M:%S "
ENV HISTFILESIZE=20000
ENV HISTFILE='/var/www/history/bash_history_php'

## Set timezone
RUN rm /etc/localtime; \
    ln -s /usr/share/zoneinfo/Europe/Moscow /etc/localtime

## File permissions
ARG PARENT_USER_ID
RUN usermod -u $PARENT_USER_ID www-data

COPY ./symfony.ini /usr/local/etc/php/conf.d
